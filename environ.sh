#!/bin/bash

export SWIFTPATH="$LANG_PATH/dist"

################################################################################

ronin_require_swift () {
    echo hello world >/dev/null
}

ronin_include_swift () {
    motd_text "    -> Swift    : "$SWIFTPATH
}

################################################################################

ronin_setup_swift () {
    echo hello world >/dev/null
}

